﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class AlbumRepository : GenericRepository<AlbumEntity>
    {
        IConnectionFactory _connectionFactory;

        public AlbumRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public List<AlbumEntity> GetAll()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_Album order by Id ";
                var param = new DynamicParameters();
                var list = connection.Query<AlbumEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

        public IEnumerable<AlbumEntity> GetByFilter(string sortColumn,string sortColumnDir)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var sqlQuery = new StringBuilder();
                sqlQuery.Append(@"
		            SELECT * FROM db_Album ");

                if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                {
                    sqlQuery.Append(@"ORDER BY " + sortColumn + " " + sortColumnDir + "");
                }

                else
                {
                    sqlQuery.Append(@"ORDER BY Id DESC");
                }


                var query = sqlQuery.ToString();
                var param = new DynamicParameters();
                var list = connection.Query<AlbumEntity>(
                    sql: query,
                    commandType: CommandType.Text,
                  
                     transaction: null,
                    param: param
                    );
                return list.AsEnumerable();
            }
        }

        public AlbumEntity GetByNo(string no)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_Album WHERE Id = '" + no + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<AlbumEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;

            }
        }
     
        public List<AlbumEntity> GetByPosition(string position)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_Album WHERE Location = '" + position + "' order by Id";
                var param = new DynamicParameters();
                var list = connection.Query<AlbumEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
                //var query = "SelectByLocation";
                //var param = new DynamicParameters();
                //param.Add("@Location", position);
                //var list = connection.Query<AlbumEntity>(
                //    sql: query,
                //    transaction: null,
                //    param: param,
                //    commandType: CommandType.StoredProcedure
                //).ToList();
                //return list;
            }
        }
        public bool Create(AlbumEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                AlbumEntity result = connection.Query<AlbumEntity>(
                    sql: "INSERT INTO db_Album (Title, Img, Description, Location ) " +
                            " VALUES (@Title, @Img, @Description, @Location) ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Update(AlbumEntity experimentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                AlbumEntity result = connection.Query<AlbumEntity>(
                    sql: "UPDATE db_Album SET Title = @Title, Img = @Img,Description = @Description, Location = @Location " +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: experimentEntity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Delete(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                AlbumEntity result = connection.Query<AlbumEntity>(
                    sql: "Delete db_Album WHERE Id =  "+ Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
        public List<AlbumEntity> GetListByName(string name)
        {
            var query = "";
            using (var connection = _connectionFactory.GetConnection)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    query = "SELECT * FROM db_Album where Title = '" + name + "' ";
                }
                else
                {
                    query = "SELECT * FROM db_Album ";
                }
                var param = new DynamicParameters();
                var list = connection.Query<AlbumEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

    }
}
