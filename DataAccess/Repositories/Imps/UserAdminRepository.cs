﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class UserAdminRepository : GenericRepository<UserAdminEntity>
    {
        IConnectionFactory _connectionFactory;

        public UserAdminRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public UserAdminEntity CheckLogin(string user, string pass)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_Admin where Username = '"+ user + "' and Password = '" + pass + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<UserAdminEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }

        public RoleEntity GetRole(int role)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM Role where Id = '" + role + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<RoleEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public RoleEntity GetByEmail(string email)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_Admin where Email = '" + email + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<RoleEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public List<RoleEntity> GetListRole(string name)
        {
            var query = "";
            using (var connection = _connectionFactory.GetConnection)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    query = "SELECT * FROM Role where NameRole = '" + name + "' ";
                }
                else
                {
                    query = "SELECT * FROM Role ";
                }
                var param = new DynamicParameters();
                var list = connection.Query<RoleEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

        public IEnumerable<UserAdminEntity> GetByFilter(string sortColumn, string sortColumnDir)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var sqlQuery = new StringBuilder();
                sqlQuery.Append(@"
		            SELECT * FROM db_Admin ");

                if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                {
                    sqlQuery.Append(@"ORDER BY " + sortColumn + " " + sortColumnDir + "");
                }

                else
                {
                    sqlQuery.Append(@"ORDER BY Id DESC");
                }


                var query = sqlQuery.ToString();
                var param = new DynamicParameters();
                var list = connection.Query<UserAdminEntity>(
                    sql: query,
                    commandType: CommandType.Text,
                    transaction: null,
                    param: param
                    );
                return list.AsEnumerable();
            }
        }

        public UserAdminEntity GetByNo(string no)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_Admin WHERE Id = '" + no + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<UserAdminEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public bool Create(UserAdminEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                UserAdminEntity result = connection.Query<UserAdminEntity>(
                    sql: "INSERT INTO db_Admin (Username, Password,Fullname, Email,Phone,Role ) " +
                            " VALUES (@Username, @Password, @Fullname, @Email, @Phone, @Role ) ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Update(UserAdminEntity experimentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                UserAdminEntity result = connection.Query<UserAdminEntity>(
                    sql: "UPDATE db_Admin SET Password = @Password, Fullname = @Fullname, Email = @Email, Phone = @Phone,Role = @Role " +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: experimentEntity
                ).FirstOrDefault();
                return result == null;
            }
        }
        public bool UpdatePass(UserAdminEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                UserAdminEntity result = connection.Query<UserAdminEntity>(
                    sql: "UPDATE db_Admin SET Password = @Password " +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }
        public bool UpdatePassByEmail(UserAdminEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                UserAdminEntity result = connection.Query<UserAdminEntity>(
                    sql: "UPDATE db_Admin SET Password = @Password " +
                      " WHERE Email =  @Email ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }
        
        public bool Delete(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                UserAdminEntity result = connection.Query<UserAdminEntity>(
                    sql: "Delete db_Admin WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
    }
}
