﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class LuotTruyCapRepository : GenericRepository<LuotTruyCapEntity>
    {
        IConnectionFactory _connectionFactory;

        public LuotTruyCapRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public LuotTruyCapEntity GetLuotXem()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_LuotTruyCap";
                var param = new DynamicParameters();
                var entity = connection.Query<LuotTruyCapEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public bool Create(LuotTruyCapEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                LuotTruyCapEntity result = connection.Query<LuotTruyCapEntity>(
                    sql: "INSERT INTO db_LuotTruyCap (DangTruyCap, TruyCapNgay,TruyCapTuan, TruyCapThang, TongLuotTruyCap ) " +
                            " VALUES (@DangTruyCap, @TruyCapNgay, @TruyCapTuan, @TruyCapThang, @TongLuotTruyCap ) ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Update(LuotTruyCapEntity experimentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                LuotTruyCapEntity result = connection.Query<LuotTruyCapEntity>(
                    sql: "UPDATE db_LuotTruyCap SET DangTruyCap = @DangTruyCap, TruyCapNgay = @TruyCapNgay, TruyCapTuan = @TruyCapTuan, TruyCapThang = @TruyCapThang, TongLuotTruyCap = @TongLuotTruyCap" +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: experimentEntity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Delete(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                LuotTruyCapEntity result = connection.Query<LuotTruyCapEntity>(
                    sql: "Delete db_LuotTruyCap WHERE Id =  "+ Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
    }
}
