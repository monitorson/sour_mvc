﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class SanPhamRepository : ISanPhamRepository
    {
        IConnectionFactory _connectionFactory;

        public SanPhamRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public List<SanPhamEntity> GetAll()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_SanPham order by Id ";
                var param = new DynamicParameters();
                var list = connection.Query<SanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public List<SanPhamEntity> GetByIdNhom(string idNhom)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_SanPham where IdNhomSanPham = "+ idNhom + " order by Id ";
                var param = new DynamicParameters();
                var list = connection.Query<SanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public List<SanPhamEntity> GetTop10()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT TOP 10 * FROM db_SanPham order by Id DESC";
                var param = new DynamicParameters();
                var list = connection.Query<SanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

        public IEnumerable<SanPhamEntity> GetByFilter(string sortColumn,string sortColumnDir, string Ten, string Active)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var sqlQuery = new StringBuilder();
                sqlQuery.Append(@"
		            SELECT * FROM db_SanPham where 1=1 ");

                if (!string.IsNullOrEmpty(Ten))
                {
                    sqlQuery.Append(@"AND TenSanPham LIKE N'%" + Ten + "%' ");
                }

                if (!string.IsNullOrEmpty(Active))
                {
                    if(Active == "1")
                    {
                        sqlQuery.Append(@"AND Active = 1 ");
                    }
                    else
                    {
                        sqlQuery.Append(@"AND Active = 0 ");
                    }
                }

                if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                {
                    sqlQuery.Append(@"ORDER BY " + sortColumn + " " + sortColumnDir + "");
                }

                else
                {
                    sqlQuery.Append(@"ORDER BY Id DESC");
                }


                var query = sqlQuery.ToString();
                var param = new DynamicParameters();
                var list = connection.Query<SanPhamEntity>(
                    sql: query,
                    commandType: CommandType.Text,
                    transaction: null,
                    param: param
                    );
                return list.AsEnumerable();
            }
        }

        public SanPhamEntity GetByNo(string no)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_SanPham WHERE Id = '" + no + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<SanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public SanPhamEntity GetByUrl(string url)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_SanPham WHERE Url = '" + url + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<SanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public bool Create(SanPhamEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                SanPhamEntity result = connection.Query<SanPhamEntity>(
                    sql: "INSERT INTO db_SanPham (TenSanPham, Url, IdNhomSanPham, Mota, Img, ChiTiet, GiaBan, GiaKhuyenMai, SeoTitle, SeoDescription, SeoKeyWord, Active ) " +
                            " VALUES (@TenSanPham, @Url, @IdNhomSanPham, @Mota, @Img, @ChiTiet, @GiaBan, @GiaKhuyenMai, @SeoTitle, @SeoDescription, @SeoKeyWord, @Active ) ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Update(SanPhamEntity experimentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                SanPhamEntity result = connection.Query<SanPhamEntity>(
                    sql: "UPDATE db_SanPham SET TenSanPham = @TenSanPham, Url = @Url, Mota = @Mota, Img =@Img, SeoTitle = @SeoTitle, SeoDescription = @SeoDescription," +
                    "SeoKeyWord = @SeoKeyWord, IdNhomSanPham= @IdNhomSanPham, ChiTiet= @ChiTiet,Active=@Active, GiaBan = @GiaBan, GiaKhuyenMai= @GiaKhuyenMai " +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: experimentEntity
                ).FirstOrDefault();
                return result == null;
            }
        }
        public bool Delete(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                SanPhamEntity result = connection.Query<SanPhamEntity>(
                    sql: "Delete db_SanPham WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
        public bool SetActiveTrue(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                SanPhamEntity result = connection.Query<SanPhamEntity>(
                    sql: "Update db_SanPham set Active = 1 WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool SetActiveFalse(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                SanPhamEntity result = connection.Query<SanPhamEntity>(
                    sql: "Update db_SanPham set Active = 0 WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
        public List<SanPhamEntity> GetListByName(string name)
        {
            var query = "";
            using (var connection = _connectionFactory.GetConnection)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    query = "SELECT * FROM db_SanPham where (TenSanPham like N'%" + name + "%' or TenSanPham like '%" + name + "%') and Active =1 ";
                }
                else
                {
                    query = "SELECT * FROM db_SanPham where Active = 1";
                }
                var param = new DynamicParameters();
                var list = connection.Query<SanPhamEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
    }
}
