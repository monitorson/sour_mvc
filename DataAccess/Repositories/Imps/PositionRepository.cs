﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class PositionRepository : GenericRepository<PositionEntity>
    {
        IConnectionFactory _connectionFactory;

        public PositionRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public List<PositionEntity> GetAll()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_Position";
                var param = new DynamicParameters();
                var list = connection.Query<PositionEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

        public IEnumerable<PositionEntity> GetByFilter(string sortColumn, string sortColumnDir)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var sqlQuery = new StringBuilder();
                sqlQuery.Append(@"
		            SELECT * FROM db_Position ");

                if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                {
                    sqlQuery.Append(@"ORDER BY " + sortColumn + " " + sortColumnDir + "");
                }

                else
                {
                    sqlQuery.Append(@"ORDER BY Id DESC");
                }


                var query = sqlQuery.ToString();
                var param = new DynamicParameters();
                var list = connection.Query<PositionEntity>(
                    sql: query,
                    commandType: CommandType.Text,
                    transaction: null,
                    param: param
                    );
                return list.AsEnumerable();
            }
        }

        public PositionEntity GetByNo(string no)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_Position WHERE Id = '" + no + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<PositionEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public bool Create(PositionEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                PositionEntity result = connection.Query<PositionEntity>(
                    sql: "INSERT INTO db_Position (Name) " +
                            " VALUES (@Name) ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Update(PositionEntity experimentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                PositionEntity result = connection.Query<PositionEntity>(
                    sql: "UPDATE db_Position SET Name = @Name" +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: experimentEntity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Delete(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                PositionEntity result = connection.Query<PositionEntity>(
                    sql: "Delete db_Position WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
    }
}
