﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class DonHangRepository : IDonHangRepository
    {
        IConnectionFactory _connectionFactory;

        public DonHangRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public List<DonHangEntity> GetAll()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_DonHang where Active =1 order by Id ";
                var param = new DynamicParameters();
                var list = connection.Query<DonHangEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public List<DonHangEntity> GetTop10()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT TOP 8 FROM db_DonHang order by Id DESC";
                var param = new DynamicParameters();
                var list = connection.Query<DonHangEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

        public IEnumerable<DonHangEntity> GetByFilter(string sortColumn,string sortColumnDir, string Ten, string Active, string DateFrom, string DateTo)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var sqlQuery = new StringBuilder();
                sqlQuery.Append(@"
		            SELECT * FROM db_DonHang where 1=1 ");

                if (!string.IsNullOrEmpty(Ten))
                {
                    sqlQuery.Append(@"AND TenSanPham LIKE N'%" + Ten + "%' ");
                }
                if (!string.IsNullOrEmpty(DateFrom) && !string.IsNullOrEmpty(DateTo))
                {
                    sqlQuery.Append(@"AND (NgayMua >= '" + DateFrom + "' AND NgayMua <= '" + DateTo + "') ");
                }

                if (!string.IsNullOrEmpty(Active))
                {
                    if(Active == "1")
                    {
                        sqlQuery.Append(@"AND Active = 1 ");
                    }
                    else
                    {
                        sqlQuery.Append(@"AND Active = 0 ");
                    }
                }

                if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                {
                    sqlQuery.Append(@"ORDER BY " + sortColumn + " " + sortColumnDir + "");
                }

                else
                {
                    sqlQuery.Append(@"ORDER BY Id DESC");
                }


                var query = sqlQuery.ToString();
                var param = new DynamicParameters();
                var list = connection.Query<DonHangEntity>(
                    sql: query,
                    commandType: CommandType.Text,
                    transaction: null,
                    param: param
                    );
                return list.AsEnumerable();
            }
        }

        public DonHangEntity GetByNo(string no)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_DonHang WHERE Id = '" + no + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<DonHangEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public List<DonHangEntity> GetByTime(string DateFrom, string DateTo, string Active)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_DonHang WHERE NgayMua >= '" + DateFrom + "' AND NgayMua <= '" + DateTo + "' and Active = '"+ Active + "'";
                var param = new DynamicParameters();
                var entity = connection.Query<DonHangEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return entity;
            }
        }

        public bool Create(DonHangEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                DonHangEntity result = connection.Query<DonHangEntity>(
                    sql: "INSERT INTO db_DonHang (TenSanPham, SoLuong, DonGia, TongGia, TenKhachHang, DiaChi, Sdt, GhiChu, LoaiThanhToan, NgayMua, Active ) " +
                            " VALUES (@TenSanPham, @SoLuong, @DonGia, @TongGia, @TenKhachHang, @DiaChi, @Sdt, @GhiChu, @LoaiThanhToan, @NgayMua, @Active ) ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Update(DonHangEntity experimentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                DonHangEntity result = connection.Query<DonHangEntity>(
                    sql: "UPDATE db_DonHang SET Active=@Active " +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: experimentEntity
                ).FirstOrDefault();
                return result == null;
            }
        }
        public bool Delete(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                DonHangEntity result = connection.Query<DonHangEntity>(
                    sql: "Delete db_DonHang WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
        public bool SetActiveTrue(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                DonHangEntity result = connection.Query<DonHangEntity>(
                    sql: "Update db_DonHang set Active = 1 WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool SetActiveFalse(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                DonHangEntity result = connection.Query<DonHangEntity>(
                    sql: "Update db_DonHang set Active = 0 WHERE Id =  " + Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
        public List<DonHangEntity> GetListByName(string name)
        {
            var query = "";
            using (var connection = _connectionFactory.GetConnection)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    query = "SELECT * FROM db_DonHang where (TenSanPham like N'%" + name + "%' or TenSanPham like '%" + name + "%') and Active =1 ";
                }
                else
                {
                    query = "SELECT * FROM db_DonHang where Active = 1";
                }
                var param = new DynamicParameters();
                var list = connection.Query<DonHangEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
    }
}
