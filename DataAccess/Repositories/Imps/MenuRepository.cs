﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class MenuRepository : GenericRepository<MenuEntity>
    {
        IConnectionFactory _connectionFactory;

        public MenuRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public List<MenuEntity> GetAll()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_Menu where Active = 1 order by Id ";
                var param = new DynamicParameters();
                var list = connection.Query<MenuEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }
        public List<MenuEntity> GetByPosition(string position)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "SELECT * FROM db_Menu where Location = "+ position + " order by OrderMenu ";
                var param = new DynamicParameters();
                var list = connection.Query<MenuEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

        public IEnumerable<MenuEntity> GetByFilter(string sortColumn,string sortColumnDir)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var sqlQuery = new StringBuilder();
                sqlQuery.Append(@"
		            SELECT * FROM db_Menu ");

                if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                {
                    sqlQuery.Append(@"ORDER BY " + sortColumn + " " + sortColumnDir + "");
                }

                else
                {
                    sqlQuery.Append(@"ORDER BY Id DESC");
                }


                var query = sqlQuery.ToString();
                var param = new DynamicParameters();
                var list = connection.Query<MenuEntity>(
                    sql: query,
                    commandType: CommandType.Text,
                    transaction: null,
                    param: param
                    );
                return list.AsEnumerable();
            }
        }

        public MenuEntity GetByNo(string no)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "Select * from db_Menu WHERE Id = '" + no + "' ";
                var param = new DynamicParameters();
                var entity = connection.Query<MenuEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).FirstOrDefault();
                return entity;
            }
        }
        public bool Create(MenuEntity entity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                MenuEntity result = connection.Query<MenuEntity>(
                    sql: "INSERT INTO db_Menu (Title, Url,Link, Detail, OrderMenu, Location,Img,ParentId,Active ) " +
                            " VALUES (@Title, @Url, @Link, @Detail, @OrderMenu, @Location, @Img,@ParentId,@Active ) ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: entity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Update(MenuEntity experimentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                MenuEntity result = connection.Query<MenuEntity>(
                    sql: "UPDATE db_Menu SET Title = @Title, Url = @Url, Link = @Link, Detail = @Detail, OrderMenu = @OrderMenu, Location = @Location, Img = @Img, ParentId = @ParentId,Active = @Active " +
                      " WHERE Id =  @Id ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: experimentEntity
                ).FirstOrDefault();
                return result == null;
            }
        }

        public bool Delete(string id)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var Id = int.Parse(id);
                MenuEntity result = connection.Query<MenuEntity>(
                    sql: "Delete db_Menu WHERE Id =  "+ Id + " ",
                    commandType: CommandType.Text,
                    transaction: null,
                    param: null
                ).FirstOrDefault();
                return result == null;
            }
        }
        public List<MenuEntity> GetListByName(string name)
        {
            var query = "";
            using (var connection = _connectionFactory.GetConnection)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    query = "SELECT * FROM db_Menu where Title = '" + name + "' ";
                }
                else
                {
                    query = "SELECT * FROM db_Menu ";
                }
                var param = new DynamicParameters();
                var list = connection.Query<MenuEntity>(
                    sql: query,
                    transaction: null,
                    param: param
                ).ToList();
                return list;
            }
        }

    }
}
