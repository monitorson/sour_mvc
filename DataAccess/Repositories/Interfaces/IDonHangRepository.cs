﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IDonHangRepository
    {
        List<DonHangEntity> GetAll();
        List<DonHangEntity> GetTop10();
        IEnumerable<DonHangEntity> GetByFilter(string sortColumn, string sortColumnDir, string Ten, string Active, string DateFrom, string DateTo);
        DonHangEntity GetByNo(string no);
        bool Create(DonHangEntity entity);
        bool Update(DonHangEntity experimentEntity);
        bool Delete(string id);
        bool SetActiveTrue(string id);
        bool SetActiveFalse(string id);
        List<DonHangEntity> GetListByName(string name);
        List<DonHangEntity> GetByTime(string DateFrom, string DateTo, string Active);
    }
}
