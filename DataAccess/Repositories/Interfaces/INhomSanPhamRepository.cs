﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface INhomSanPhamRepository
    {
        List<NhomSanPhamEntity> GetAll();
        List<NhomSanPhamEntity> GetTop10();
        IEnumerable<NhomSanPhamEntity> GetByFilter(string sortColumn, string sortColumnDir,string Ten, string Active);
        NhomSanPhamEntity GetByNo(string no);
        bool Create(NhomSanPhamEntity entity);
        bool Update(NhomSanPhamEntity experimentEntity);
        bool Delete(string id);
        bool SetActiveTrue(string id);
        bool SetActiveFalse(string id);
        List<NhomSanPhamEntity> GetListByName(string name);
        List<NhomSanPhamEntity> GetListNoiBat();
        int GetIdByUrl(string url);
        List<NhomSanPhamEntity> GetListTrangChu();
    }
}
