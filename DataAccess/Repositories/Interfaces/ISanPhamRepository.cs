﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface ISanPhamRepository
    {
        List<SanPhamEntity> GetAll();
        List<SanPhamEntity> GetTop10();
        IEnumerable<SanPhamEntity> GetByFilter(string sortColumn, string sortColumnDir,string Ten, string Active);
        SanPhamEntity GetByNo(string no);
        bool Create(SanPhamEntity entity);
        bool Update(SanPhamEntity experimentEntity);
        bool Delete(string id);
        bool SetActiveTrue(string id);
        bool SetActiveFalse(string id);
        List<SanPhamEntity> GetListByName(string name);
        List<SanPhamEntity> GetByIdNhom(string idNhom);
        SanPhamEntity GetByUrl(string url);
    }
}
