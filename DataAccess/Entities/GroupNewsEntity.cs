﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class GroupNewsEntity
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        public string Url { get; set; }

        public string Img { get; set; }
        public string Position { get; set; }
        public bool Active { get; set; }
        public List<NewsEntity> ListNews { get; set; } = new List<NewsEntity>();
    }
}
