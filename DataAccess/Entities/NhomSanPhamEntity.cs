﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class NhomSanPhamEntity
    {
        public int? Id { get; set; }
        public string TenNhom { get; set; }
        public string Url { get; set; }
        public string Mota { get; set; }
        public string Img { get; set; }
        public string SeoTitle { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeyWord { get; set; }
        public bool HienThiTrangChu { get; set; }
        public bool HienThiNoiBat { get; set; }
        public bool Active { get; set; }
    }
}
