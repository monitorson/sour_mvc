﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class SanPhamEntity
    {
        public int? Id { get; set; }
        public string TenSanPham { get; set; }
        public string Url { get; set; }
        public string IdNhomSanPham { get; set; }
        public string Img { get; set; }
        public string SeoTitle { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeyWord { get; set; }
        public bool Active { get; set; }
        public string Mota { get; set; }
        public string ChiTiet { get; set; }
        public double GiaBan { get; set; }
        public double GiaKhuyenMai { get; set; }
        public string SoLuong { get; set; }
    }
}
