﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class MenuEntity
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Link { get; set; }
        public string Detail { get; set; }
        public int? OrderMenu { get; set; }
        public string Location { get; set; }
        public string Img { get; set; }
        public int ParentId { get; set; }
        public bool Active { get; set; }
    }
}
