﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class LuotTruyCapEntity
    {
        public int? Id { get; set; }
        public int DangTruyCap { get; set; }
        public int TruyCapNgay { get; set; }
        public int TruyCapTuan { get; set; }
        public int TruyCapThang { get; set; }
        public int TongLuotTruyCap { get; set; }
    }
}
