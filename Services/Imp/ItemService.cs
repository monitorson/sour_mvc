﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DataAccess.Entities;
using DataAccess.Repositories;
using DataAccess.Infrastructure;

namespace Services.Imp
{
    public class ItemService : IExperimentService
    {
        private ExperimentRepository _experimentRepository = null;
        private QualityMeasurementRepository _qualityMeasurementRepository = null;
        IConnectionFactory _connectionFactory;
        public ItemService(
            ExperimentRepository experimentRepository,
            QualityMeasurementRepository qualityMeasurementRepository,
            IConnectionFactory connectionFactory)
        {
            _experimentRepository = experimentRepository;
            _qualityMeasurementRepository = qualityMeasurementRepository;
            _connectionFactory = connectionFactory;
        }

        /// <summary>
        /// Create a Item
        /// </summary>
        /// <param name="itemEntity"></param>
        /// <param name="itemDetailEntity"></param>
        /// <param name="itemImageEntities"></param>
        /// <returns></returns>
        public ExperimentEntity Create(ExperimentEntity experimentEntity, IList<QualityMeasurementEntity> qualityMeasuermentEntity)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                try
                {
                    var item = _experimentRepository.Create(experimentEntity);

                    if (string.IsNullOrEmpty(item.依頼No))
                    {
                        return null;
                    }
                    else
                    {

                        if (qualityMeasuermentEntity != null)
                        {
                            foreach (var itemQualityEntity in qualityMeasuermentEntity)
                            {
                                itemQualityEntity.依頼No = item.依頼No;
                                var quality = _qualityMeasurementRepository.Create(itemQualityEntity);
                                if (string.IsNullOrEmpty(quality.管理No))
                                {
                                    return null;
                                }
                            }
                        }

                        return item;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Update a Item
        /// </summary>
        /// <param name="itemEntity"></param>
        /// <param name="itemDetailEntity"></param>
        /// <param name="itemImageEntities"></param>
        /// <param name="itemMemoEntities"></param>
        /// <returns></returns>
        public ExperimentEntity Update(ExperimentEntity experimentEntity, IList<QualityMeasurementEntity> qualityMeasuermentEntity)
        {
            //using (var transaction = new TransactionScope())
            //{
            //    try
            //    {
            //        var item = _iItemsRepository.Update(itemEntity);

            //        if (item.Id <= 0)
            //        {
            //            return null;
            //        }
            //        else
            //        {
            //            // detail
            //            itemDetailEntity.ItemCode = item.ItemCode;
            //            var itemDetail = _iItemDetailsRepository.UpSert(itemDetailEntity);
            //            if (itemDetail.Id <= 0)
            //            {
            //                return null;
            //            }

            //            // delete tags
            //            string entity = typeof(ItemEntity).Name;
            //            int entityId = item.Id;
            //            _iTagRepository.DeleteAllByEntity(entity, entityId);

            //            // add tags
            //            foreach (var itemTagEntity in itemTagEntities)
            //            {
            //                itemTagEntity.EntityId = item.Id;
            //                var tag = _iTagRepository.Create(itemTagEntity);
            //                if (tag.Id <= 0)
            //                {
            //                    return null;
            //                }
            //            }

            //            // ======= hang tags ======
            //            #region
            //            // itemLabel
            //            itemLabelEntity.ItemCode = item.ItemCode;
            //            var itemLabel = _itemLabelsRepository.UpSert(itemLabelEntity);
            //            if (itemLabel.Id <= 0)
            //            {
            //                return null;
            //            }

            //            // itemLabelsMaterial + ItemLabelsMaterialsDetailEntity
            //            if (itemLabelEntity != null)
            //            {
            //                // DEL all ItemLabelsMaterialsDetail and ItemLabelsMaterial and ItemLabelsWashings
            //                _itemLabelsMaterialsDetailsRepository.DeleteByItemLabelsId(itemLabelEntity.Id);
            //                _itemLabelsMaterialsRepository.DeleteByItemLabelsId(itemLabelEntity.Id);
            //                _itemLablesWashingsRepository.DeleteByItemLabelsId(itemLabelEntity.Id);

            //                // ItemLabelsWashings
            //                foreach (var ItemLabelsWashing in itemLabelEntity.ItemLabelsWashings)
            //                {
            //                    ItemLabelsWashing.ItemLabelsId = itemLabel.Id;
            //                    var itemLabelsWashingEntity = _itemLablesWashingsRepository.Create(ItemLabelsWashing);
            //                    if (itemLabelsWashingEntity.Id <= 0)
            //                    {
            //                        return null;
            //                    }
            //                }

            //                foreach (var ItemLabelsMaterial in itemLabelEntity.ItemLabelsMaterials)
            //                {
            //                    // itemLabelsMaterial
            //                    ItemLabelsMaterial.ItemLabelsId = itemLabel.Id;
            //                    var itemLabelsMaterialEntity = _itemLabelsMaterialsRepository.UpSert(ItemLabelsMaterial);
            //                    if (itemLabelsMaterialEntity.Id <= 0)
            //                    {
            //                        return null;
            //                    }

            //                    // ItemLabelsMaterialsDetailEntity
            //                    foreach (var ItemLabelsMaterialsDetail in ItemLabelsMaterial.ItemLabelsMaterialsDetails)
            //                    {
            //                        ItemLabelsMaterialsDetail.ItemLabelsMaterialsId = itemLabelsMaterialEntity.Id;
            //                        var itemLabelsMaterialsDetail = _itemLabelsMaterialsDetailsRepository.UpSert(ItemLabelsMaterialsDetail);
            //                        if (itemLabelsMaterialsDetail.Id <= 0)
            //                        {
            //                            return null;
            //                        }
            //                    }
            //                }

            //                // ItemLablesAttachedTerms, ItemLablesAuxiliaryMaterials, ItemLabelsCareLabels, ItemLabelsInnerCareLabels
            //                #region
            //                // delete all ItemLablesAttachedTerms, ItemLablesAuxiliaryMaterials, ItemLabelsCareLabels, ItemLabelsInnerCareLabels
            //                _itemLablesAttachedTermsRepository.DeleteByItemLabelsId(itemLabelEntity.Id);
            //                _itemLablesAuxiliaryMaterialsRepository.DeleteByItemLabelsId(itemLabelEntity.Id);
            //                _itemLabelsCareLabelsRepository.DeleteByItemLabelsId(itemLabelEntity.Id);
            //                _itemLabelsInnerCareLabelsRepository.DeleteByItemLabelsId(itemLabelEntity.Id);

            //                foreach (var ItemLablesAttachedTerm in itemLabelEntity.ItemLablesAttachedTerms)
            //                {
            //                    var itemLablesAttachedTerm = _itemLablesAttachedTermsRepository.Create(new ItemLablesAttachedTermEntity
            //                    {
            //                        ItemLabelsId = itemLabel.Id,
            //                        AttachedTermCode = ItemLablesAttachedTerm.AttachedTermCode
            //                    });
            //                    if (itemLablesAttachedTerm.Id <= 0)
            //                    {
            //                        return null;
            //                    }
            //                }
            //                foreach (var ItemLablesAuxiliaryMaterial in itemLabelEntity.ItemLablesAuxiliaryMaterials)
            //                {
            //                    var itemLablesAuxiliaryMaterials = _itemLablesAuxiliaryMaterialsRepository.Create(new ItemLablesAuxiliaryMaterialEntity
            //                    {
            //                        ItemLabelsId = itemLabel.Id,
            //                        AuxiliaryMaterialsCode = ItemLablesAuxiliaryMaterial.AuxiliaryMaterialsCode
            //                    });
            //                    if (itemLablesAuxiliaryMaterials.Id <= 0)
            //                    {
            //                        return null;
            //                    }
            //                }
            //                foreach (var ItemLabelsCareLabel in itemLabelEntity.ItemLabelsCareLabels)
            //                {
            //                    var itemLabelsCareLabel = _itemLabelsCareLabelsRepository.Create(new ItemLabelsCareLabelEntity
            //                    {
            //                        ItemLabelsId = itemLabel.Id,
            //                        AttensionCode = ItemLabelsCareLabel.AttensionCode
            //                    });
            //                    if (itemLabelsCareLabel.Id <= 0)
            //                    {
            //                        return null;
            //                    }
            //                }
            //                foreach (var ItemLabelsInnerCareLabel in itemLabelEntity.ItemLabelsInnerCareLabels)
            //                {
            //                    var itemLabelsInnerCareLabel = _itemLabelsInnerCareLabelsRepository.Create(new ItemLabelsInnerCareLabelEntity
            //                    {
            //                        ItemLabelsId = itemLabel.Id,
            //                        AttensionCode = ItemLabelsInnerCareLabel.AttensionCode
            //                    });
            //                    if (itemLabelsInnerCareLabel.Id <= 0)
            //                    {
            //                        return null;
            //                    }
            //                }

            //                #endregion
            //            }
            //            // ======= end hang tags ======
            //            #endregion
            //            var itemOrderOld = _iItemImagesRepository.GetItemLastByItemcode(item.Id.ToString());
            //            var OrderOld = itemOrderOld != null ? itemOrderOld.Order : 0;
            //            // image
            //            foreach (var itemImageEntity in itemImageEntities)
            //            {
            //                itemImageEntity.ItemCode = item.ItemCode;
            //                itemImageEntity.Order = ++OrderOld;
            //                var image = _iItemImagesRepository.Create(itemImageEntity);
            //                if (image.Id <= 0)
            //                {
            //                    return null;
            //                }
            //            }

            //            //item Memo Create
            //            itemMemoEntity.ItemCode = item.ItemCode;
            //            if (!string.IsNullOrEmpty(itemMemoEntity.Memo) && _iItemMemosRepository.Create(itemMemoEntity).Id <= 0)
            //            {
            //                return null;
            //            }

            //            transaction.Complete();
            //            return item;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        return null;
            //    }
            //}
            return null;
        }
    }
}