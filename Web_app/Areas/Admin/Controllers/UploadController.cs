﻿using DataAccess.Entities;
using DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Web_app.Common;
using Web_app.Models;
using Web_app.Utilities;

namespace DailyOpt_CROP4A.Areas.Admin.Controllers
{
    public class UploadController : Controller
    {
        public UploadController()
        {

        }

        private static Random _random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[_random.Next(s.Length)]).ToArray());
        }

        [HttpPost]
        public string ProcessUpload(HttpPostedFileBase file, string type)
        {
            string yearMonthDay = DateTime.Now.ToString(Constant.FORMAT_DATE_DDMMYYYY).Replace("-", "");
            string extension = Path.GetExtension(file.FileName);
            var name = yearMonthDay + "_" + RandomString(15) + extension;
            string path = Constant.PATH_IMG;
            switch (type)
            {
                case "Album":
                    if (CheckFileTypeImg(file.FileName) == false)
                    {
                        return "";
                    }
                    path = Constant.PATH_ALBUM;
                    break;
                case "File":
                    if (CheckFileType(file.FileName) == false)
                    {
                        return "";
                    }
                    path = Constant.PATH_FILE;
                    break;
                case "Html":
                    if (CheckFileType(file.FileName) == false)
                    {
                        return "";
                    }
                    path = Constant.PATH_HTML;
                    break;
                default:
                    if (CheckFileTypeImg(file.FileName) == false)
                    {
                        return "";
                    }
                    break;
            }
            string fileName = string.Format("{0}\\{1}", path, name.ToLower());

            file.SaveAs(Server.MapPath(fileName));
            return fileName;
        }

        bool CheckFileTypeImg(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".gif":
                    return true;
                case ".jpg":
                    return true;
                case ".jpeg":
                    return true;
                case ".png":
                    return true;
                default:
                    return false;
            }
        }

        bool CheckFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".docx":
                    return true;
                case ".doc":
                    return true;
                case ".xlsx":
                    return true;
                case ".xls":
                    return true;
                case ".pdf":
                    return true;
                case ".html":
                    return true;
                default:
                    return false;
            }
        }
    }
}