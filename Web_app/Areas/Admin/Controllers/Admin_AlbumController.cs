﻿using DataAccess.Entities;
using DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Web_app.Common;
using Web_app.Models;
using Web_app.Utilities;

namespace DailyOpt_CROP4A.Areas.Admin.Controllers
{
    public class Admin_AlbumController : Controller
    {
        private AlbumRepository _AlbumRepository = null;
        private PositionRepository _positionRepository = null;
        public Admin_AlbumController(AlbumRepository AlbumRepository, PositionRepository positionRepository)
        {
            _AlbumRepository = AlbumRepository;
            _positionRepository = positionRepository;

        }
        public ActionResult Album(AlbumViewModel model)
        {
            model.Locations = _positionRepository.GetAll();
            return View(model);
        }

        [HttpPost]
        public ActionResult Album_Read()
        {
            try
            {
                //Creating instance of DatabaseContext class  
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //Paging Size (10,20,50,100)    
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var listData = _AlbumRepository.GetByFilter(sortColumn, sortColumnDir);
                //total number of rows count     
                recordsTotal = listData.Count();
                //Paging     
                var datamodel = listData.OrderByDescending(c => c.Id).Skip(skip).Take(pageSize).ToList();
                //Returning Json Data    
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = datamodel });
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Album_Read: " + ex.Message);
                throw;
            }

        }


        public ActionResult Album_Preview(string Id)
        {
            try
            {
                var data = _AlbumRepository.GetByNo(Id);

                var dataList = ParseEntityToModel(data);

                return Json(new { result = dataList });

            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Album_Preview: " + ex.Message);
                throw;
            }

        }

        private AlbumViewModel ParseEntityToModel(AlbumEntity entity)
        {
            try
            {
                var AlbumViewModel = new AlbumViewModel
                {
                    Id = entity.Id,
                    Title = entity.Title,
                    Description = entity.Description,
                    Img = entity.Img,
                    Location = entity.Location,
                };
                return AlbumViewModel;
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("ParseEntityToModel: " + ex.Message);
                throw;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add_Album(AlbumViewModel model)
        {
            try
            {
                //1. nếu model.Id null => insert
                //2. nếu model.Id not null => update
                if (string.IsNullOrEmpty(model.Id.ToString()))
                {

                    if (ModelState.IsValid)
                    {

                        var entity = new AlbumEntity();
                        entity.Title = model.Title;
                        entity.Img = model.Img;
                        entity.Location = model.Location;
                        entity.Description = model.Description;
                        var img = _AlbumRepository.Create(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-Album");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-Album");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-Album");
                    }
                }
                else
                {
                    //update
                    if (ModelState.IsValid)
                    {
                        var entity = new AlbumEntity();
                        entity.Id = model.Id;
                        entity.Title = model.Title;
                        entity.Img = model.Img;
                        entity.Location = model.Location;
                        entity.Description = model.Description;
                        var img = _AlbumRepository.Update(entity);
                        if (img)
                        {
                            TempData["MessageSuccess"] = "Cập nhật thông tin thành công";
                            return RedirectToAction("Add-Album");
                        }
                        else
                        {
                            //insert error
                            TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                            return RedirectToAction("Add-Album");
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                        return RedirectToAction("Add-Album");
                    }
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Add_Album: " + ex.Message);
                TempData["MessageError"] = "Có lỗi xảy ra, vui lòng kiểm tra lại";
                return RedirectToAction("Add-Album");
            }

        }

        [HttpPost]
        public ActionResult Delete_Album(string Id)
        {
            try
            {
                var data = _AlbumRepository.Delete(Id);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Delete_Album: " + ex.Message);
                throw;
            }

        }

        [HttpPost]
        public ActionResult Coppy_Album(string Id)
        {
            try
            {
                var model = _AlbumRepository.GetByNo(Id);
                var entity = new AlbumEntity();
                entity.Title = model.Title;
                entity.Img = model.Img;
                entity.Description = model.Description;
                entity.Location = model.Location;
                var data = _AlbumRepository.Create(entity);
                if (data)
                {
                    return Json(new { result = "Success" });
                }
                else
                {
                    return Json(new { result = "Error" });
                }
            }
            catch (Exception ex)
            {
                Log4NetCommon.Error("Coppy_Album: " + ex.Message);
                throw;
            }

        }
    }
}