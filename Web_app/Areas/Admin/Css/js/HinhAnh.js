﻿
$(function () {
    $("#ListAdvertisement").DataTable({
        "lengthMenu": [[5, 20, 50, 100], [5, 20, 50, 100]],
        "serverSide": true,
        "filter": false,
        "orderable": false,
        "pageLength": 5,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "language": {
            "lengthMenu": "Hiển thị _MENU_ trên một trang",
            "zeroRecords": "Không có bản ghi nào",
            "info": "page _PAGE_ of _PAGES_",
            "infoEmpty": "Data trống",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "info": "Từ _START_ đến _END_ trên tổng _TOTAL_ bản ghi",
            "oPaginate": {
                "sPrevious": "Quay lại",
                "sNext": "Tiếp theo"
            }
        },
        "ajax": {
            "url": "/Admin_Advertisement/show-img",
            "type": "POST",
            "datatype": "json",

        },
        "order": [[0, 'asc']],
        "columns": [
            { "data": "Id", "name": "Id" },
            { "data": "Title", "name": "Title" },
            {
                "orderable": false,
                "data": null,
                "name": "Img",
                render: function (data, type, row) {
                    if (data.Img === null || data.Img === "") {
                        return '<img src="/Img/no-image-100.png" style="max-height: 100px;" />';
                    }
                    else {
                        return '<img src="' + data.Img + '" style="max-height: 100px;" />';
                    }
                },
                "targets": -1
            },
            { "data": "Link", "name": "Link" },
            {
                "orderable": false,
                "data": null,
                "name": "DisplayActive",
                render: function (data, type, row) {
                    if (data.Active === true) {
                        return '<i class="fa fa-check-circle" style="color:green"></i>';
                    }
                    else {
                        return '<i class="fa fa-minus-circle" style="color:red"></i>';
                    }
                },
                "targets": -1
            },
            {
                "orderable": false,
                "data": null,
                "name": "Delete",
                "searchable": false,
                render: function (data, type, row) {
                    return '<i class="fa fa-plus" style="color:green;cursor: pointer;" onclick="CoppyImg(' + data.Id + ');" title="Nhân bản"></i>&nbsp&nbsp <i class="fa fa-trash-o" style="color:red;cursor: pointer;" onclick="DeleteImg(' + data.Id + ');" title="Xóa"></i>';
                },
                "targets": -1
            }
        ]
    });
});
$(document).ready(function () {
    var table = $('#ListAdvertisement').DataTable();
    $('#ListAdvertisement tbody').on('click', 'td:not(:last-child)', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var data = table.row(this.closest('tr')).data();
        $("#Id").val(data["Id"]);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_Advertisement/get-info',
            data: JSON.stringify({ Id: data["Id"] }),
            dataType: 'json',

            success: function (result) {
                $('#modal-img').modal('show');
                $("#Location").val(result.result.Location);
                $("input[name=Title]").val(result.result.Title);
                $("input[name=Link]").val(result.result.Link);
                $("#AnhDaiDien").attr('src', result.result.Img);
                $("#Image").val(result.result.Img);
                if (result.result.Active === 1) {
                    $("#Active").prop("checked", true);
                }
                else {
                    $("#Active").prop("checked", false);
                }
                CKEDITOR.instances['Desciption'].setData(result.result.Desciption);
                $("input[name=OrderImg]").val(result.result.OrderImg);
            }
        });

    });
});

function ShowModalAdd() {
    $("input[name=Title]").val("");
    $("input[name=Link]").val("");
    $("#AnhDaiDien").attr('src', "");
    $("#Image").val("");
    CKEDITOR.instances['Desciption'].setData("");
    $("#Location").val("0");
    $("input[name=OrderImg]").val("");
    $("#Active").prop("checked", true);
    $('#modal-img').modal('show');
}
function DeleteImg(Id) {
    if (confirm("Bạn có chắc chắn muốn xóa?")) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_Advertisement/delete-img',
            data: JSON.stringify({ Id: Id }),
            dataType: 'json',
            success: function (result) {
                if (result.result === "Success") {
                    $("#MesageDelete").css("display", "block");
                    var table = $('#ListAdvertisement').DataTable();
                    table.draw();
                }

            }
        });
    }
} 
function CoppyImg(Id) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Admin_Advertisement/coppy-img',
        data: JSON.stringify({ Id: Id }),
        dataType: 'json',
        success: function (result) {
            if (result.result === "Success") {
                var table = $('#ListAdvertisement').DataTable();
                table.draw();
            }

        }
    });
} 

