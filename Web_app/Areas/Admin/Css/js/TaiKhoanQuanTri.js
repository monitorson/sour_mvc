﻿$(document).ready(function () {
    $(".chose-role").select2({
        ajax: {
            url: '/User_Admin/Role_Read',
            width: 'resolve',
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    name: params.term,
                    requestForm: "edit"
                };
            },
            processResults: function (data) {
                return { results: data.DataList };
            }
        }
    });
});

$(function () {
    $("#ListUser").DataTable({
        "lengthMenu": [[5, 20, 50, 100], [5, 20, 50, 100]],
        "serverSide": true,
        "filter": false,
        "orderable": false,
        "pageLength": 5,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "language": {
            "lengthMenu": "Hiển thị _MENU_ trên một trang",
            "zeroRecords": "Không có bản ghi nào",
            "info": "page _PAGE_ of _PAGES_",
            "infoEmpty": "Data trống",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "info": "Từ _START_ đến _END_ trên tổng _TOTAL_ bản ghi",
            "oPaginate": {
                "sPrevious": "Quay lại",
                "sNext": "Tiếp theo"
            }
        },
        "ajax": {
            "url": "/User_Admin/show-list-user",
            "type": "POST",
            "datatype": "json",

        },
        "order": [[0, 'asc']],
        "columns": [
            { "data": "Id", "name": "Id" },
            { "data": "Username", "name": "Username" },
            { "data": "Fullname", "name": "Fullname" },
            { "data": "Email", "name": "Email" },
            { "orderable": false, "data": "DisplayRole", "name": "DisplayRole" },
             {
                "orderable": false,
                "data": null,
                "name": "Delete",
                 render: function (data, type, row) {
                     return '<i class="fa fa-trash-o" style="color:red;cursor: pointer;" onclick="DeleteUser(' + data.Id + ');"></i>';
                },
                "targets": -1
            }
        ]
    });
});
 $(document).ready(function () {
     var table = $('#ListUser').DataTable();
     $('#ListUser tbody').on('click', 'td:not(:last-child)', function () {
         if ($(this).hasClass('selected')) {
             $(this).removeClass('selected');
         }
         else {
             table.$('tr.selected').removeClass('selected');
             $(this).addClass('selected');
         }
         var data = table.row(this.closest('tr')).data();
        $("#Id").val(data["Id"]);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/User_Admin/get-info-user',
            data: JSON.stringify({ Id: data["Id"] }),
            dataType: 'json',

            success: function (result) {
                $('#modal-admin-user').modal('show');
                $("input[name=Fullname]").val(result.result.Fullname);
                $("input[name=Username]").val(result.result.Username);
                $("input[name=Password]").val(result.result.Password);
                $("input[name=Email]").val(result.result.Email);
                $("input[name=Phone]").val(result.result.Phone);
                var $newOption = $("<option selected='selected'></option>").val(result.result.Role).text(result.result.DisplayRole)
                $("#Role").append($newOption).trigger('change');
            }
        });

    });

   
 });
function ShowModalAdd() {
    $("input[name=Fullname]").val("");
    $("input[name=Username]").val("");
    $("input[name=Password]").val("");
    $("input[name=Email]").val("");
    $("input[name=Phone]").val("");
    $('#modal-admin-user').modal('show');
}
function DeleteUser(Id) {
    if (confirm("Bạn có chắc chắn muốn xóa?")) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/User_Admin/delete-user-admin',
            data: JSON.stringify({ Id: Id }),
            dataType: 'json',
            success: function (result) {
                if (result.result === "Success") {
                    $("#MesageDelete").css("display", "block");
                    var table = $('#ListUser').DataTable();
                    table.draw();
                }

            }
        });
    }
}

