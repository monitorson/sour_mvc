﻿
$(function () {
    $("#ListAlbum").DataTable({
        "lengthMenu": [[5, 20, 50, 100], [5, 20, 50, 100]],
        "serverSide": true,
        "filter": false,
        "orderable": false,
        "pageLength": 5,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "language": {
            "lengthMenu": "Hiển thị _MENU_ trên một trang",
            "zeroRecords": "Không có bản ghi nào",
            "info": "page _PAGE_ of _PAGES_",
            "infoEmpty": "Data trống",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "info": "Từ _START_ đến _END_ trên tổng _TOTAL_ bản ghi",
            "oPaginate": {
                "sPrevious": "Quay lại",
                "sNext": "Tiếp theo"
            }
        },
        "ajax": {
            "url": "/Admin_Album/Album_Read",
            "type": "POST",
            "datatype": "json",

        },
        "order": [[0, 'asc']],
        "columns": [
            { "data": "Id", "name": "Id" },
            { "data": "Title", "name": "Title" },
            {
                "orderable": false,
                "data": null,
                "name": "Delete",
                "searchable": false,
                render: function (data, type, row) {
                    return '<i class="fa fa-plus" style="color:green;cursor: pointer;" onclick="CoppyAlbum(' + data.Id + ');" title="Nhân bản"></i>&nbsp&nbsp <i class="fa fa-trash-o" style="color:red;cursor: pointer;" onclick="DeleteAlbum(' + data.Id + ');" title="Xóa"></i>';
                },
                "targets": -1
            }
        ]
    });
});
$(document).ready(function () {
    var table = $('#ListAlbum').DataTable();
    $('#ListAlbum tbody').on('click', 'td:not(:last-child)', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var data = table.row(this.closest('tr')).data();
        $("#Id").val(data["Id"]);
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_Album/Album_Preview',
            data: JSON.stringify({ Id: data["Id"] }),
            dataType: 'json',

            success: function (result) {
                $('#modal-img').modal('show');
                $("#Location").val(result.result.Location);
                $("input[name=Title]").val(result.result.Title);

                $("#ListImgView").empty();
                $("#ListImg").val("");

                var listImg = result.result.Img.split(",");
                listImg.splice(-1, 1);
                for (var i = 0; i < listImg.length; i++) {
                    $("#ListImgView").css("display", "block");
                    $("#ListImg").val($("#ListImg").val() + listImg[i] + ",");
                    $("#ListImgView").append("<img id='img" + i + "' src='" + listImg[i] + "' class='img-album' style='max-width:100%;max-height:100px;margin: 10px;float: left;' /><button type='button' style='border: none; background: none;float: left;' id='ok" + i + "' title='" + listImg[i] + "' onclick='deleteImgAlbum(\"" + i + "\");'><i class='ti-close'></i></button>")
                }

                CKEDITOR.instances['Description'].setData(result.result.Description);
            }
        });

    });
});

function ShowModalAdd() {
    $("input[name=Title]").val("");
    //$("#AnhDaiDien").attr('src', "");
    //$("#Image").val("");
    CKEDITOR.instances['Description'].setData("");
    $("#Location").val("0");
    $('#modal-img').modal('show');
}
function DeleteAlbum(Id) {
    if (confirm("Bạn có chắc chắn muốn xóa?")) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_Album/Delete_Album',
            data: JSON.stringify({ Id: Id }),
            dataType: 'json',
            success: function (result) {
                if (result.result === "Success") {
                    $("#MesageDelete").css("display", "block");
                    var table = $('#ListAlbum').DataTable();
                    table.draw();
                }

            }
        });
    }
}
function CoppyAlbum(Id) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Admin_Album/Coppy_Album',
        data: JSON.stringify({ Id: Id }),
        dataType: 'json',
        success: function (result) {
            if (result.result === "Success") {
                var table = $('#ListAlbum').DataTable();
                table.draw();
            }

        }
    });
}

