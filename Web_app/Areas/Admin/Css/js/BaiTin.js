﻿$(document).ready(function () {
    $(".chose-group-news").select2({
        ajax: {
            url: '/Admin_News/show-nhom-tin-select',
            width: 'resolve',
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    name: params.term,
                    requestForm: "edit"
                };
            },
            processResults: function (data) {
                return { results: data.DataList };
            }
        }
    });
});

$(function () {
    $("#ListNews").DataTable({
        "lengthMenu": [[5, 20, 50, 100], [5, 20, 50, 100]],
        "serverSide": true,
        "filter": false,
        "orderable": false,
        "pageLength": 5,
        //"scrollY": false,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',

        "language": {
            "lengthMenu": "Hiển thị _MENU_ trên một trang",
            "zeroRecords": "Không có bản ghi nào",
            "info": "page _PAGE_ of _PAGES_",
            "infoEmpty": "Data trống",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "info": "Từ _START_ đến _END_ trên tổng _TOTAL_ bản ghi",
            "oPaginate": {
                "sPrevious": "Quay lại",
                "sNext": "Tiếp theo"
            }
        },
        "ajax": {
            "url": "/Admin_News/show-bai-tin",
            "type": "POST",
            "datatype": "json",
            "data": function (d) {
                return $.extend({}, d, {
                    "IdGroupNews": $("#IdGroupNewsSearch").val(),
                    "Active": $("#ActiveSearch").val(), 
                    "DateCreateFrom": $("#DateCreateFromSearch").val(),
                    "DateCreateTo": $("#DateCreateToSearch").val(),
                    "Title": $("#TitleSearch").val()
                });
            }
        },
        "order": [[0, 'asc']],
        "columns": [
            { "data": "Id", "name": "Id" },
            { "data": "Title", "name": "Title" },
            {
                "orderable": false,
                "data": null,
                "name": "Img",
                render: function (data, type, row) {
                    if (data.Img === null || data.Img === "") {
                        return '<img src="/Img/no-image-100.png" style="max-height: 100px;" />';
                    }
                    else {
                        return '<img src="' + data.Img + '" style="max-height: 100px;" />';
                    }
                },
                "targets": -1
            },
            { "data": "DateCreate", "name": "DateCreate" },
            { "data": "UserCreate", "name": "UserCreate" },
            {
                "orderable": false,
                "data": null,
                "name": "DisplayActive",
                render: function (data, type, row) {
                    if (data.Active === true) {
                        return '<i class="fa fa-check-circle" style="color:green"></i>';
                    }
                    else {
                        return '<i class="fa fa-minus-circle" style="color:red"></i>';
                    }
                },
                "targets": -1
            },
            {
                "orderable": false,
                "data": null,
                "name": "Delete",
                "searchable": false,
                render: function (data, type, row) {
                    return '<i class="fa fa-plus" style="color:green;cursor: pointer;" onclick="CoppyNews(' + data.Id + ');" title="Nhân bản"></i>&nbsp&nbsp <i class="fa fa-trash-o" style="color:red;cursor: pointer;" onclick="DeleteGroupNews(' + data.Id + ');" title="Xóa"></i>';
                },
                "targets": -1
            }
        ]
    });
});
$(document).ready(function () {
    var table = $('#ListNews').DataTable();
    $('#ListNews tbody').on('click', 'td:not(:last-child)', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var data = table.row(this.closest('tr')).data();
        $("#Id").val(data["Id"]);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_News/get-info-news',
            data: JSON.stringify({ Id: data["Id"] }),
            dataType: 'json',

            success: function (result) {
                $('#modal-news').modal('show');
                var $newOption = $("<option selected='selected'></option>").val(result.result.IdGroupNews).text(result.result.DisplayGroupNews)
                $("#IdGroupNews").append($newOption).trigger('change');
                $("input[name=Title]").val(result.result.Title);
                $("input[name=Url]").val(result.result.Url);
                $("#AnhDaiDien").attr('src', result.result.Img);
                $("#Image").val(result.result.Img);
                //$("#FileDinhKem").val(result.result.FileDinhKem);
                $("#FileHtml").val(result.result.FileHtml);
                $("#DateCreate").val(result.result.DateCreate);
                $("input[name=UserCreate]").val(result.result.UserCreate);

                $("#ListImgView").empty();
                $("#FileDinhKem").val("");
                if (result.result.FileDinhKem != null) {
                    var listImg = result.result.FileDinhKem.split(",");
                    listImg.splice(-1, 1);
                    for (var i = 0; i < listImg.length; i++) {
                        $("#ListImgView").css("display", "block");
                        $("#FileDinhKem").val($("#FileDinhKem").val() + listImg[i] + ",");
                        $("#ListImgView").append("<span id='file" + i + "' style='color: #153bf5;font-size: 11px;font-weight: bold'>" + listImg[i] + "<button type='button' style='border: none; background: none;float: left;font-size: 11px;' id='ok" + i + "' title='" + listImg[i] + "' onclick='deleteFile(\"" + i + "\");'><i class='ti-close'></i></button> </span> <br/>")
                    }
                }
                else {
                    $("#FileDinhKem").val("");
                }

                if (result.result.Active === 1) {
                    $("#Active").prop("checked", true);
                }
                else {
                    $("#Active").prop("checked", false);
                }
                $('#Description').text(result.result.Description);

                if (result.result.CongTacTuan !== null) {
                    CKEDITOR.instances["DetailLichCongTac"].setData(result.result.CongTacTuan);
                }
                else {
                    CKEDITOR.instances["Detail"].setData(result.result.Detail);
                }
                
                $("input[name=UrlVideo]").val(result.result.UrlVideo);
                $("input[name=SeoKeyWord]").val(result.result.SeoKeyWord);
                $("input[name=SeoTitle]").val(result.result.SeoTitle);
                $("input[name=OrderNews]").val(result.result.OrderNews);
                $("input[name=SeoDesciption]").val(result.result.SeoDesciption);
                if (result.result.IsNew === 1) {
                    $("#IsNew").prop("checked", true);
                }
                else {
                    $("#IsNew").prop("checked", false);
                }
                $("#Position").val(result.result.Position);
            }
        });

    });


});
function SetUrl(value) {
    var name = value;
    var str = xoa_dau_tieng_viet(value);
    var value = str.replace(/\s+/g, '-').toLowerCase();
    $("input[name=Url]").val(value);
    $("input[name=SeoKeyWord]").val(name);
    $("input[name=SeoTitle]").val(name);
    $("input[name=SeoDesciption]").val(name);
}
function ShowModalAdd() {
    $("input[name=Title]").val("");
    $("input[name=Url]").val("");
    $("#AnhDaiDien").attr('src', "");
    $("#Image").val("");
    $("textarea[name=Description]").val("");
    $("#FileDinhKem").val("");
    $('#Detail').css("display", "block");
    CKEDITOR.replace('Detail');
    CKEDITOR.instances["Detail"].setData("");

    CKEDITOR.replace('DetailLichCongTac');
    CKEDITOR.instances["DetailLichCongTac"].destroy();
    $('#DetailLichCongTac').css("display", "none");

    $("#ListImgView").empty();
    $("input[name=UrlVideo]").val("");
    $("input[name=SeoKeyWord]").val("");
    $("input[name=SeoTitle]").val("");
    $("input[name=SeoDesciption]").val("");
    $("input[name=OrderNews]").val("");
    $("#IsNew").prop("checked", false);
    $("#Active").prop("checked", true);
    $("#Position").val("0");
    $("input[name=UserCreate]").val("");
    $('#modal-news').modal('show');
}
function DeleteGroupNews(Id) {
    if (confirm("Bạn có chắc chắn muốn xóa?")) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Admin_News/delete-news',
            data: JSON.stringify({ Id: Id }),
            dataType: 'json',
            success: function (result) {
                if (result.result === "Success") {
                    $("#MesageDelete").css("display", "block");
                    var table = $('#ListNews').DataTable();
                    table.draw();
                }

            }
        });
    }
    else {
    }
} 
function CoppyNews(Id) {

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Admin_News/coppy-news',
        data: JSON.stringify({ Id: Id }),
        dataType: 'json',
        success: function (result) {
            if (result.result === "Success") {
                var table = $('#ListNews').DataTable();
                table.draw();
            }

        }
    });
} 


function SearchNews() {
    var table = $('#ListNews').DataTable();
    table.draw();
}


function SetLicCongTac(thisId) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Admin_News/get-info-lich-cong-tac',
        data: JSON.stringify({ Id: thisId }),
        dataType: 'json',

        success: function (result) {
            if (result.result === "Success") {
               
                $('#DetailLichCongTac').css("display", "block");
                CKEDITOR.replace('DetailLichCongTac');
                CKEDITOR.instances["DetailLichCongTac"].setData(result.data);

                CKEDITOR.replace('Detail');
                CKEDITOR.instances["Detail"].destroy();
                $('#Detail').css("display", "none");
            }
            else {
                ///
                CKEDITOR.replace('DetailLichCongTac');
                CKEDITOR.instances["DetailLichCongTac"].setData("");
                CKEDITOR.instances["DetailLichCongTac"].destroy();
                $('#DetailLichCongTac').css("display", "none");

                CKEDITOR.replace('Detail');
                $('#Detail').css("display", "block");
                CKEDITOR.instances["Detail"].setData("");
            }
        }
    });



}
