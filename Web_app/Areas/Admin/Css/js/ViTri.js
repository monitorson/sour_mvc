﻿$(function () {
    $("#ListPosition").DataTable({
        "lengthMenu": [[5, 20, 50, 100], [5, 20, 50, 100]],
        "serverSide": true,
        "filter": false,
        "orderable": false,
        "pageLength": 5,
        //"scrollY": false,
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "language": {
            "lengthMenu": "Hiển thị _MENU_ trên một trang",
            "zeroRecords": "Không có bản ghi nào",
            "info": "page _PAGE_ of _PAGES_",
            "infoEmpty": "Data trống",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "info": "Từ _START_ đến _END_ trên tổng _TOTAL_ bản ghi",
            "oPaginate": {
                "sPrevious": "Quay lại",
                "sNext": "Tiếp theo"
            }
        },
        "ajax": {
            "url": "/HomeAdmin/Position_Read",
            "type": "POST",
            "datatype": "json",

        },
        "order": [[0, 'asc']],
        "columns": [
            { "data": "Id", "name": "Id" },
            { "data": "Name", "name": "Name" },
             {
                "orderable": false,
                "data": null,
                "name": "Delete",
                 render: function (data, type, row) {
                     return '<i class="fa fa-trash-o" style="color:red;cursor: pointer;" onclick="DeletePos(' + data.Id + ');"></i>';
                },
                "targets": -1
            }
        ]
    });
});
 $(document).ready(function () {
     var table = $('#ListPosition').DataTable();
     $('#ListPosition tbody').on('click', 'td:not(:last-child)', function () {
         if ($(this).hasClass('selected')) {
             $(this).removeClass('selected');
         }
         else {
             table.$('tr.selected').removeClass('selected');
             $(this).addClass('selected');
         }
         var data = table.row(this.closest('tr')).data();
        $("#Id").val(data["Id"]);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/HomeAdmin/Position_Preview',
            data: JSON.stringify({ Id: data["Id"] }),
            dataType: 'json',

            success: function (result) {
                $('#modal-position').modal('show');
                $("input[name=Name]").val(result.result.Name);
            }
        });

    });

 });

function ShowModalAdd() {
    $("input[Id=Id]").val("");
    $("input[name=Name]").val("");
    $("input[name=Url]").val("");
    $("#AnhDaiDien").attr('src', "");
    $("#Image").val("");
    $("#Active").prop("checked", true);
    $("#Position").val("0");
    $('#modal-position').modal('show');
}
function DeletePos(Id) {
    if (confirm("Bạn có chắc chắn muốn xóa?")) {
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/HomeAdmin/Delete_Position',
            data: JSON.stringify({ Id: Id }),
            dataType: 'json',
            success: function (result) {
                if (result.result === "Success") {
                    $("#MesageDelete").css("display", "block");
                    var table = $('#ListPosition').DataTable();
                    table.draw();
                }

            }
        });
    }
}

