/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	 config.language = 'vi';
	// config.uiColor = '#AADC6E';
	//config.filebrowserBrowseUrl = '/ckfinder/ckfinder.html';
    config.entities_latin = false;
    config.allowedContent = true;

    config.filebrowserBrowseUrl = "/upAnh.html";
    config.filebrowserWindowWidth = 500;
    config.filebrowserWindowHeight = 650;

    //config.extraPlugins = "image";
    //ilebrowserBrowseUrl = '/browser/browse.php';
    //filebrowserUploadUrl = '/uploader/upload.php';
};
