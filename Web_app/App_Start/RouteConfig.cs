﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Web_app
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
              name: "DetailNews",
              "Detail/{cate}/{type}",
              new { controller = "News", action = "ChiTietBaiViet" },
              new[] { "Code.Web.Controllers" }
            );
            routes.MapRoute(
            "Contact",
            "lien-he",
            new { controller = "Home", action = "LienHe" },
            new[] { "Code.Web.Controllers" }
            );
            routes.MapRoute(
           "Search",
           "tim-kiem",
           new { controller = "News", action = "TimKiem" },
           new[] { "Code.Web.Controllers" }
           );
            routes.MapRoute(
          "AnhHoatDong",
          "anh-hoat-dong",
          new { controller = "News", action = "AnhHoatDong" },
          new[] { "Code.Web.Controllers" }
          );
            routes.MapRoute(
              "GroupNews",
              "{type}",
              new { controller = "News", action = "DanhSachBaiViet" },
              new[] { "Code.Web.Controllers" }
            );
            routes.MapRoute(
            "PhanTrang",
            "page/News/PartialNews",
            new { controller = "News", action = "PartialNews" },
            new[] { "Code.Web.Controllers" }
            );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

           


        }
    }
}
