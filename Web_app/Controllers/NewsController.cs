﻿using DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_app.Common;
using Web_app.Models;
using DataAccess.Entities;
using System.Text;
using System.IO;

namespace DailyOpt_CROP4A.Controllers
{
    public class NewsController : Controller
    {
        private NewsRepository _newsRepository = null;
        private GroupNewsRepository _groupNewsRepository = null;
        private AlbumRepository _albumRepository = null;

        public NewsController(NewsRepository newsRepository, GroupNewsRepository groupNewsRepository, AlbumRepository albumRepository)
        {
            _newsRepository = newsRepository;
            _groupNewsRepository = groupNewsRepository;
            _albumRepository = albumRepository;
        }
        // GET: News
        public ActionResult Search()
        {
            return View();
        }
        public ActionResult AlbumHome()
        {
            var model = _albumRepository.GetByPosition(Constant.POSITION_HOME).OrderByDescending(c => c.Id).ToList();
            if (model == null)
            {
                model = new List<AlbumEntity>();
            }
            return View(model);
        }
        public ActionResult AlbumHomeMobile()
        {
            var model = _albumRepository.GetByPosition(Constant.POSITION_HOME);
            if (model == null)
            {
                model = new List<AlbumEntity>();
            }
            return View(model);
        }

        public ActionResult TinCotPhai()
        {
            var model = _groupNewsRepository.GetByPosition(Constant.COT_PHAI);
            if (model != null)
            {
                foreach (var item in model)
                {
                    item.ListNews = _newsRepository.GetByGroupNews(item.Id.ToString()).OrderByDescending(c => c.DateCreate).ThenByDescending(c => c.Id).ToList();
                }
            }
            else
            {
                model = new List<GroupNewsEntity>();
            }
           

            return View(model);
        }
        private NewsViewModel ParseObjNewsEntityToModel(NewsEntity entity)
        {
            var groupNews = _groupNewsRepository.GetByNo(entity.IdGroupNews.ToString());
            var newsViewModel = new NewsViewModel
            {
                Id = entity.Id,
                Title = entity.Title,
                Url = entity.Url,
                Img = entity.Img,
                DateCreate = entity.DateCreate,
                DateDelete = entity.DateDelete,
                DateUpdate = entity.DateUpdate,
                Description = entity.Description,
                Detail = entity.Detail,
                IdGroupNews = entity.IdGroupNews,
                IsNew = entity.IsNew == true ? 1 : 0,
                OrderNews = entity.OrderNews,
                SeoDesciption = entity.SeoDesciption,
                SeoKeyWord = entity.SeoKeyWord,
                SeoTitle = entity.SeoTitle,
                UrlVideo = entity.UrlVideo,
                UserCreate = entity.UserCreate,
                UserDelete = entity.UserDelete,
                UserUpdate = entity.UserUpdate,
                DisplayGroupNews = groupNews != null ? groupNews.Name : "",
                UrlGroupNews = groupNews != null ? groupNews.Url : "",
                Position = entity.Position,
                Video = entity.Video,
                Active = entity.Active == true ? 1 : 0,
                FileHtml = entity.FileHtml,
                FileDinhKem = entity.FileDinhKem,
                CongTacTuan = entity.CongTacTuan,
                SoLuotXem = entity.SoLuotXem,
            };
            return newsViewModel;
        }
        public ActionResult CotPhai()
        {
            return View();
        }
        public ActionResult NewsHome()
        {
            var data = _groupNewsRepository.GetAll().Where(g => g.Position == Constant.POSITION_HOME).OrderByDescending(c => c.Id).Take(6).ToList();
            if (data.Count() > 0)
            {
                foreach (var item in data)
                {
                    item.ListNews = _newsRepository.GetByGroupNews(item.Id.ToString()).OrderByDescending(c => c.DateCreate).ThenByDescending(c => c.Id).ToList();
                }
            }
            else
            {
                data = new List<GroupNewsEntity>();
            }
            return View(data);
        }

        public ActionResult ChiTietBaiViet(string type)
        {
            var entity = _newsRepository.GetAll().Where(c => c.Url.Equals(type.ToLower())).FirstOrDefault();
            _newsRepository.UpdateLuotXem(entity.Id.ToString());
            entity = _newsRepository.GetByNo(entity.Id.ToString());
            var model = ParseObjNewsEntityToModel(entity);

            if (!string.IsNullOrEmpty(entity.FileHtml))
            {
                string path = HttpContext.Server.MapPath(entity.FileHtml);
                string readText = System.IO.File.ReadAllText(path);
                model.FileHtml = readText;
            }
            return View(model);
        }
        public ActionResult BaiVietLienQuan(int IdGroupNews, int IdNews)
        {
            var newitem = _newsRepository.GetAll().Where(c => c.Id != IdNews && c.IdGroupNews == IdGroupNews).OrderByDescending(c => c.DateCreate).ThenByDescending(c => c.Id).Take(8);
            var listModelNews = new List<NewsViewModel>();
            foreach (var item in newitem)
            {
                var model = ParseObjNewsEntityToModel(item);
                listModelNews.Add(model);
            }
            return View(listModelNews);
        }

        public ActionResult DanhSachBaiViet(string type)
        {
            var data = _groupNewsRepository.GetAll().Where(g => g.Url.Equals(type)).FirstOrDefault();
            if (data != null)
            {
                data.ListNews = _newsRepository.GetByGroupNews(data.Id.ToString()).OrderByDescending(c => c.DateCreate).ThenByDescending(c => c.Id).ToList();
            }
            else
            {
                data = new GroupNewsEntity();
            }
            return View(data);
        }
        [HttpPost]
        public ActionResult TimKiem(string key)
        {
            var data = _newsRepository.SearchTitle(key);
            var listModelNews = new List<NewsViewModel>();
            foreach (var item in data)
            {
                var model = ParseObjNewsEntityToModel(item);
                listModelNews.Add(model);
            }
            return View(listModelNews);
        }

        public ActionResult PartialNews(int id, int page)
        {
            var newsitem = _newsRepository.GetByGroupNews(id.ToString()).OrderByDescending(c => c.DateCreate).ThenByDescending(c => c.Id);
            var lst = _newsRepository.NewsPage(page, 6, id.ToString(), newsitem.ToList());
            var listModelNews = new List<NewsViewModel>();
            foreach (var item in lst)
            {
                var model = ParseObjNewsEntityToModel(item);
                listModelNews.Add(model);
            }
            var currentUrl = Url.Action("PartialNews", "News", new { id = id });
            ViewBag.Htmlpage = new HtmlPager().getHtmlPageForum(currentUrl + "&page=", 1, page, 6, newsitem.Count());
            return View(listModelNews);
        }
        public ActionResult TinSlideDuoiMenu()
        {
            var model = _newsRepository.GetAll().Where(g => g.Position == Constant.DUOI_MENU_TRANG_CHU).OrderByDescending(c => c.DateCreate).ThenByDescending(c=>c.Id).ToList();
            var listModelNews = new List<NewsViewModel>();
            if (model != null)
            {
                foreach (var item in model)
                {
                    var modelParse = ParseObjNewsEntityToModel(item);
                    listModelNews.Add(modelParse);
                }
            }
            return View(listModelNews);
        }
        public ActionResult TinSlideDuoiMenuBenPhai()
        {
            var model = _newsRepository.GetAll().Where(g => g.Position == Constant.BEN_PHAI_DUOI_MENU_TRANG_CHU).OrderByDescending(c => c.Id).Take(8).ToList();
            var listModelNews = new List<NewsViewModel>();
            if (model != null)
            {
                foreach (var item in model)
                {
                    var modelParse = ParseObjNewsEntityToModel(item);
                    listModelNews.Add(modelParse);
                }
            }
            return View(listModelNews);
        }

        public ActionResult ThongBao()
        {
            var model = _newsRepository.GetAll().Where(g => g.Position == Constant.THONG_BAO_TRANG_CHU).OrderByDescending(c => c.DateCreate).ThenByDescending(c => c.Id).ToList();
            var listModelNews = new List<NewsViewModel>();
            if (model != null)
            {
                foreach (var item in model)
                {
                    var modelParse = ParseObjNewsEntityToModel(item);
                    listModelNews.Add(modelParse);
                }
            }
            return View(listModelNews);
        }
        public ActionResult TinMoi()
        {
            var model = _newsRepository.GetAll().OrderByDescending(c => c.DateCreate).ThenByDescending(c => c.Id).Take(20).ToList();
            var listModelNews = new List<NewsViewModel>();
            if (model != null)
            {
                foreach (var item in model)
                {
                    var modelParse = ParseObjNewsEntityToModel(item);
                    listModelNews.Add(modelParse);
                }
            }
            return View(listModelNews);
        }
        public ActionResult AnhHoatDong()
        {
            var model = _albumRepository.GetAll().OrderByDescending(c => c.Id).ToList();
            if (model == null)
            {
                model = new List<AlbumEntity>();
            }
            return View(model);
        }
    }
}