﻿using Web_app.Utilities;
using System.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Linq;
using Web_app.Common;
using DataAccess.Repositories;
using Web_app.Models;
using System.Collections.Generic;
using System;
using System.Text;
using DataAccess.Entities;
using Services.Imp;

using System.IO;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Data;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;


namespace Web_app.Controllers
{
    public class HomeController : Controller
    {
        private ConfigRepository _configRepository = null;
        private AdvertisementRepository _advertisementRepository = null;
        private MenuRepository _menuRepository = null;
        private AlbumRepository _albumRepository = null;
        private LuotTruyCapRepository _luotTruyCapRepository = null;

        public HomeController(ConfigRepository configRepository, AdvertisementRepository advertisementRepository,
            MenuRepository menuRepository, AlbumRepository albumRepository, LuotTruyCapRepository luotTruyCapRepository)
        {
            _configRepository = configRepository;
            _advertisementRepository = advertisementRepository;
            _menuRepository = menuRepository;
            _albumRepository = albumRepository;
            _luotTruyCapRepository = luotTruyCapRepository;
        }
        [HttpGet]
        public ActionResult Index()
        {
            var entiyConfig = _configRepository.GetInfo();
            var model = new ConfigViewModel();
            model.Company = entiyConfig.Company;
            model.SeoKeyWord = entiyConfig.SeoKeyWord;
            model.SeoDesciption = entiyConfig.SeoDesciption;
            model.SeoTitle = entiyConfig.SeoTitle;

            if (Session["log"] != null)
            {
                var luot = _luotTruyCapRepository.GetLuotXem();
                DateTime date = DateTime.Now;
                var thisDay = DateTime.Parse(System.IO.File.ReadAllText(Server.MapPath("ThisDay.txt")));
                // lấy ra giờ của ngày hiện tại -> nếu từ 0-24h thì update cột ngày
                //var thisHour = date.ToString("hh:mm");
                //var hourDay = "23:59";
                //int t1 = Int32.Parse(thisHour.Replace(":", ""));
                //int t2 = Int32.Parse(hourDay.Replace(":", ""));
                //var comp = 0;
                //if(t1 == t2) { comp = 1; }
                if (date.Date == thisDay.Date)
                {
                    luot.TruyCapNgay = luot.TruyCapNgay + 1;
                }
                else
                {
                    luot.TruyCapNgay = 1;
                    System.IO.File.WriteAllText(Server.MapPath("ThisDay.txt"), DateTime.Now.Date.ToString());
                }
                // Lấy ngày hiện tại của tuần -> nếu trong khoảng ngày đầu và ngày cuối của tuần thì update tuần
                if (date >= datetimeext.GetFirstDayOfWeek(date) && date <= datetimeext.GetLasttDayOfWeek(date))
                {
                    luot.TruyCapTuan = luot.TruyCapTuan + 1;
                }
                else
                {
                    luot.TruyCapTuan = 1;
                }

                // Lấy ngày hiện tại của tháng -> nếu trong khoảng ngày đầu và ngày cuối của tháng thì update tuần
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                if (date >= firstDayOfMonth && date <= lastDayOfMonth)
                {
                    luot.TruyCapThang = luot.TruyCapThang + 1;
                }
                else
                {
                    luot.TruyCapThang = 1;
                }

                luot.TongLuotTruyCap = luot.TongLuotTruyCap + 1;
                _luotTruyCapRepository.Update(luot);
                Session["log"] = null;
            }
            return View(model);
        }

        public ActionResult Logo()
        {
            var entity = _advertisementRepository.GetByPosition(Constant.POSITION_LOGO);
            return View(entity);
        }
        public ActionResult ThongKeTruyCap()
        {
            var entity = _luotTruyCapRepository.GetLuotXem();
            ViewBag.DaTruyCap = HttpContext.Application["DaTruyCap"].ToString();
            ViewBag.DangTruyCap = HttpContext.Application["DangTruyCap"].ToString();
            return View(entity);
        }
        public ActionResult ModalSendMail()
        {
            return View();
        }
        public ActionResult Menu()
        {
            var entity = _menuRepository.GetByPosition(Constant.POSITION_MENU_TOP);
            return View(entity);
        }
        public ActionResult MenuMobile()
        {
            var entity = _menuRepository.GetByPosition(Constant.POSITION_MENU_TOP);
            return View(entity);
        }
        public ActionResult Social()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult SocialFooter()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult Slide()
        {
            var entity = _advertisementRepository.GetByPosition(Constant.POSITION_SLIDE);
            return View(entity);
        }
        public ActionResult AnhQuangCaoDuoiVideo()
        {
            var model = _albumRepository.GetAll().OrderByDescending(c => c.Id).Take(1).FirstOrDefault();
            if (model == null)
            {
                model = new AlbumEntity();
            }
            return View(model);
        }

        public ActionResult Codebody()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult CodeHeader()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult PageDetail(string Id)
        {
            var model = _menuRepository.GetByNo(Id);
            return View(model);
        }
        public ActionResult SocialRight()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult FanPage()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }
        public ActionResult RegisterEmail()
        {
            return View();
        }
        public ActionResult DoiTac()
        {
            var entity = _advertisementRepository.GetByPosition(Constant.POSITION_DOI_TAC);
            return View(entity);
        }
        public ActionResult LienHe()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }

        public ActionResult VideoGioiThieu()
        {
            var entity = _configRepository.GetInfo();
            return View(entity);
        }

        [HttpPost]
        public ActionResult SendEmail(string email)
        {
            try
            {
                var infoEmailTo = _configRepository.GetInfo().EmailInfo;
                SendMail(null, email, null, null, infoEmailTo);
                return Json(new { result = "Success" });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [HttpPost]
        public ActionResult SendEmailContact(string name, string email, string phone, string message)
        {
            try
            {
                var infoEmailTo = _configRepository.GetInfo().EmailInfo;
                SendMail(name, email, phone, message, infoEmailTo);
                return Json(new { result = "Success" });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        static async Task SendMail(string name, string email, string phone, string message, string infoEmailTo)
        {
            var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("suport@c3trunggia.edu.vn", "Info");
            var to = new EmailAddress();
            var subject = "";
            var plainTextContent = "";
            var htmlContent = "";

            if (string.IsNullOrEmpty(name))
            {
                subject = "Thông tin người dùng để lại trên trang c3trunggia.edu.vn";
                to = new EmailAddress(infoEmailTo, "THPT Trung Giã");
                htmlContent = "<strong>Email người dùng đăng ký: </strong> " + email + " ";
            }
            else
            {
                subject = "Thông tin người dùng để lại trên trang c3trunggia.edu.vn";
                to = new EmailAddress(infoEmailTo, "THPT Trung Giã");
                htmlContent = "<strong>Họ tên: </strong> " + name + " <br /><strong>Email người dùng đăng ký: </strong> " + email + "<br /><strong>Số điện thoại: </strong> " + phone + " <br /><strong>Nội dung: </strong> " + message + "";
            }
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }


    }
}
