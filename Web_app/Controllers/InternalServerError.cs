﻿using System.Web.Mvc;

namespace Web_app.Controllers
{
    public class InternalServerError : Controller
    {
        
        [HttpGet]
        public ActionResult Error()
        {
            return View();
        }
    }
}
