﻿using System.Collections.Generic;

namespace Web_app.Common
{
	public class MenuData
	{
		public List<string> Items { get; set; } = new List<string>();
	}
}