﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web_app.Common
{
    public class Constant : ApiController
    {
        public const string POSITION_LOGO = "1";

        public const string POSITION_SLIDE = "2";

        public const string POSITION_LOGO_FOOTER = "3";

        public const string POSITION_QC_TRAI = "4";

        public const string POSITION_QC_PHAI = "5";

        public const string POSITION_DOI_TAC = "6";

        public const string POSITION_MENU_TOP = "1";

        public const string POSITION_MENU_BOTTOM = "2";

        public const string POSITION_MENU_LEFT = "3";

        public const string POSITION_MENU_RIGHT = "4";

        public const string POSITION_RIGHT = "2";

        public const string POSITION_LEFT = "3";

        public const string POSITION_HOME = "5";

        public const string COT_PHAI = "2";
        public const string COT_TRAI = "3";
        public const string TRANG_CHU = "5";
        public const string DUOI_MENU_TRANG_CHU = "6";
        public const string BEN_PHAI_DUOI_MENU_TRANG_CHU = "7";
        public const string THONG_BAO_TRANG_CHU = "8";
        public const string PATH_ALBUM = @"\Uploads\Album";
        public const string PATH_FILE = @"\Uploads\File";
        public const string PATH_HTML = @"\Uploads\FileHtml";
        public const string PATH_IMG = @"\Uploads\Images";
        public const string FORMAT_DATE_DDMMYYYY = "dd/MM/yyyy";

    }
}