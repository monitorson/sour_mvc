﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web_app.Common
{
    public class HtmlPager
    {
        /// <summary>
        /// Phân trang cho gridView
        /// </summary>
        /// <param name="strPathPage">Tiền tố trước page. Có dạng #parram1=value&...</param>
        /// <param name="intCurrentPage">Trang hiện tại</param>
        /// <param name="intRowPerPage">SỐ bản ghi trên 1 trang</param>
        /// <param name="intTotalRecord">Tổng số bản ghi</param>
        /// <returns>Mã html phân trang</returns>
        /// <modified>
        /// Author				created date					comments
        /// dongdt				07/06/2011					    Tạo mới
        ///</modified>
        public static string getPage(string strPathPage, int intCurrentPage, int intRowPerPage, int intTotalRecord)
        {
            var ltsRowPerpage = new List<int>() { 5, 10, 15, 20, 25, 30, 35, 40, 45, 50 };
            if (!ltsRowPerpage.Contains(intRowPerPage))
                ltsRowPerpage.Add(intRowPerPage);
            ltsRowPerpage.Sort();

            int intTotalPage = (intTotalRecord % intRowPerPage == 0) ? intTotalRecord / intRowPerPage : ((intTotalRecord - (intTotalRecord % intRowPerPage)) / intRowPerPage) + 1; ;
            var strBuilder = new StringBuilder();
            if (intTotalRecord > 0)
            {
                strBuilder.Append("<div class=\"bottom-pager\">\r\n");
                strBuilder.Append("    <div class=\"left\">\r\n");
                if (intCurrentPage > 1)
                {
                    strBuilder.AppendFormat("        <a href=\"{0}1\" class=\"first\" title=\"Trang đầu\"></a>\r\n", strPathPage);
                    strBuilder.AppendFormat("        <a href=\"{0}{1}\" class=\"pre\" title=\"Trang trước\"></a>\r\n", strPathPage, intCurrentPage - 1);
                }
                else
                {
                    strBuilder.Append("        <a href=\"javascript:;\" class=\"first-disable\" title=\"Trang đầu\"></a>\r\n");
                    strBuilder.Append("        <a href=\"javascript:;\" class=\"pre-disable\" title=\"Trang trước\"></a>\r\n");
                }
                strBuilder.Append("        <span>Trang</span>\r\n");
                strBuilder.AppendFormat("        <input type=\"text\" name=\"page\" value=\"{0}\" />\r\n", intCurrentPage);
                strBuilder.AppendFormat("        <input type=\"hidden\" value=\"{0}\" />\r\n", intTotalPage);
                strBuilder.AppendFormat("        <span>/{0}</span>\r\n", intTotalPage);

                if (intCurrentPage < intTotalPage)
                {
                    strBuilder.AppendFormat("        <a href=\"{0}{1}\" class=\"next\" title=\"Trang tiếp\"></a>\r\n", strPathPage, intCurrentPage + 1);
                    strBuilder.AppendFormat("        <a href=\"{0}{1}\" class=\"last\" title=\"Trang cuối\"></a>\r\n", strPathPage, intTotalPage);
                }
                else
                {
                    strBuilder.Append("        <a href=\"javascript:;\" class=\"next-disable\" title=\"Trang tiếp\"></a>\r\n");
                    strBuilder.Append("        <a href=\"javascript:;\" class=\"last-disable\" title=\"Trang cuối\"></a>\r\n");
                }
                strBuilder.Append("    </div>\r\n");
                strBuilder.Append("    <div class=\"right\">\r\n");
                strBuilder.Append("        <span>Kết quả trên 1 trang:</span>\r\n");
                strBuilder.Append("        <select name=\"RowPerPage\">\r\n");
                foreach (var item in ltsRowPerpage)
                {
                    strBuilder.AppendFormat("            <option value=\"{0}\"{1}>{2}</option>\r\n", item, (item == intRowPerPage) ? " selected" : "", item);
                }
                strBuilder.Append("        </select>\r\n");
                strBuilder.AppendFormat("        <span>/ Tổng số: {0}</span>\r\n", intTotalRecord);
                strBuilder.Append("    </div>\r\n");
                strBuilder.Append("</div>\r\n");
            }
            else
            {
                strBuilder.Append("<div class=\"bottom-pager\"><span>Hiện tại danh sách này chưa có dữ liệu.</span></div>\r\n");
            }
            return strBuilder.ToString();
        }
        #region Các biến xử dụng
        private int _PageStep;
        private int _CurrentPage;
        private string _LinkPage;
        private int _TotalPage;
        private string _LinkPageExt;
        #endregion

        #region Các thuộc tính
        public int PageStep
        {
            get { return _PageStep; }
            set { _PageStep = value; }
        }

        public int TotalPage
        {
            get { return _TotalPage; }
            set { _TotalPage = value; }
        }
        public int CurrentPage
        {
            get { return _CurrentPage; }
            set { _CurrentPage = value; }
        }

        public string LinkPage
        {
            get { return _LinkPage; }
            set { _LinkPage = value; }
        }
        public string LinkPageExt
        {
            get { return _LinkPageExt; }
            set { _LinkPageExt = value; }
        }
        #endregion

        /// <summary>
        /// Hàm Constructer
        /// </summary>
        /// <Modified>        
        ///	Name		Date		    Comment 
        /// dongdt     10/11/2011      Tạo mới
        /// </Modified>
        public HtmlPager()
        {
            CurrentPage = 1;
            LinkPage = string.Empty;
            TotalPage = 1;
            PageStep = 3;
            LinkPageExt = "";
        }

        /// <summary>
        /// Hàm lấy về mã html phân trang
        /// </summary>
        /// <param name="_LinkPage">đường link của trang</param>
        /// <param name="_CurrentPage">Trang hiện tại</param>
        /// <param name="_RowPerPage">Số bản ghi trên trang</param>
        /// <param name="_TotalRow">Tổng số bản ghi</param>
        /// <Modified>        
        ///	Name		Date		    Comment 
        /// Tuantm     10/11/2011      Tạo mới
        /// </Modified>
        public string getHtmlPage(string _LinkPage, int _PageStep, int _CurrentPage, int _RowPerPage, int _TotalRow)
        {
            this.PageStep = _PageStep;
            CurrentPage = _CurrentPage;
            LinkPage = _LinkPage;
            if (_RowPerPage == 0)
                _RowPerPage = 5;
            TotalPage = (_TotalRow % _RowPerPage == 0) ? _TotalRow / _RowPerPage : ((_TotalRow - (_TotalRow % _RowPerPage)) / _RowPerPage) + 1;
            return WriteHTMLPage();
        }

        /// <summary>
        /// Hàm lấy về phân trang, hỗ trợ cho urlRewrite
        /// </summary>
        /// <param name="_LinkPage">đường link của trang - Phía trước page</param>
        /// <param name="_LinkPageExt">đường link của trang - Phía sau page</param>
        /// <param name="_CurrentPage">Trang hiện tại</param>
        /// <param name="_RowPerPage">Số bản ghi trên trang</param>
        /// <param name="_TotalRow">Tổng số bản ghi</param>
        /// <Modified>        
        ///	Name		Date		    Comment 
        /// Tuantm     10/11/2011      Tạo mới
        /// </Modified>
        public string getHtmlPage(string _LinkPage, string _LinkPageExt, int _PageStep, int _CurrentPage, int _RowPerPage, int _TotalRow)
        {
            this.PageStep = _PageStep;
            CurrentPage = _CurrentPage;
            LinkPage = _LinkPage;
            LinkPageExt = _LinkPageExt;
            TotalPage = (_TotalRow % _RowPerPage == 0) ? _TotalRow / _RowPerPage : ((_TotalRow - (_TotalRow % _RowPerPage)) / _RowPerPage) + 1;
            return WriteHTMLPage();
        }

        /// <summary>
        /// Hàm write mã HTML phân trang
        /// </summary>
        /// <Modified>        
        ///	Name		Date		    Comment 
        /// dongdt     10/11/2011      Tạo mới
        /// </Modified>
        private string WriteHTMLPage()
        {
            string strPageHTML = "<li>";

            if (CurrentPage > PageStep + 1)
            {
                //strPageHTML += "<a href=\"" + LinkPage + 1 + LinkPageExt + "\">««</a>";
                strPageHTML += "<a class =\"prev page-number\" href=\"" + LinkPage + (CurrentPage - 1) + LinkPageExt + "\"><i class=\"icon-angle-left\"></i></a>";
                strPageHTML += "<a class=\"page-number\">...</a>";
            }

            int BeginFor = ((CurrentPage - PageStep) > 1) ? (CurrentPage - PageStep) : 1;
            int EndFor = ((CurrentPage + PageStep) > TotalPage) ? TotalPage : (CurrentPage + PageStep);

            for (int pNumber = BeginFor; pNumber <= EndFor; pNumber++)
            {
                if (pNumber == CurrentPage)
                    strPageHTML += "<span aria-current=\"page\" class=\"page-number current\">" + pNumber + "</span>";
                else
                    strPageHTML += "<a class=\"page-number\" href=\"" + LinkPage + pNumber + LinkPageExt + "\">" + pNumber + "</a>";
            }

            if (CurrentPage < (TotalPage - PageStep))
            {
                strPageHTML += "<a class=\"page-number\">...</a>";
                strPageHTML += "<a class=\"next page-number\" href=\"" + LinkPage + (CurrentPage + 1) + LinkPageExt + "\"><i class=\"icon-angle-right\"></i></a>";
                //strPageHTML += "<a href=\"" + LinkPage + TotalPage + LinkPageExt + "\">»»</a>";

            }
            strPageHTML += "</li>";
            if (TotalPage > 1)
                return strPageHTML;
            else
                return string.Empty;
        }
        /// <summary>
        /// Hàm write mã HTML phân trang
        /// </summary>
        /// <Modified>        
        ///	Name		Date		    Comment 
        /// dongdt     10/11/2011      Tạo mới
        /// </Modified>
        private string WriteHTMLPageForum()
        {
            string strPageHTML = "";

            int BeginFor = ((CurrentPage - PageStep) > 1) ? (CurrentPage - PageStep) : 1;
            int EndFor = ((CurrentPage + PageStep) > TotalPage) ? TotalPage : (CurrentPage + PageStep);
            if (CurrentPage > PageStep + 1)
            {
                //strPageHTML += "<a href=\"" + LinkPage + 1 + LinkPageExt + "\">««</a>";
                strPageHTML += "<a class =\"prev page-number\" href=\"" + LinkPage + (CurrentPage - 1) + LinkPageExt + "\"><i class=\"icon-angle-left\"></i></a>";
                strPageHTML += "<a class=\"page-number\">...</a>";
            }

            for (int pNumber = BeginFor; pNumber <= EndFor; pNumber++)
            {
                if (pNumber == CurrentPage)
                    //strPageHTML += "<li><a class=\"prev page-number\" href=\"javascript:;\"> <i class=\"icon-angle-left\"></i></a></li>";
                strPageHTML += "<li><span aria-current =\"page\" class=\"page-number current\">" + pNumber + "</span></li>";
                else
                    strPageHTML += "<li><a class=\"page-number\" href=\"" + LinkPage + pNumber + LinkPageExt + "\">" + pNumber + "</a></li>";
                    //strPageHTML += "<li><a class=\"next page-number\" href=\"" + LinkPage + pNumber + LinkPageExt + "\"> <i class=\"icon-angle-right\"></i></a></li>";
                
            }

            if (CurrentPage < (TotalPage - PageStep))
            {
                strPageHTML += "<li><a class=\"next page-number\" href=\"" + LinkPage + TotalPage + LinkPageExt + "\"><i class=\"icon-angle-right\"></i></a></li>";

            }
            strPageHTML += "";
            if (TotalPage > 1)
                return strPageHTML;
            else
                return string.Empty;
        }

        /// <summary>
        /// Hàm lấy về mã html phân trang
        /// </summary>
        /// <param name="_LinkPage">đường link của trang</param>
        /// <param name="_CurrentPage">Trang hiện tại</param>
        /// <param name="_RowPerPage">Số bản ghi trên trang</param>
        /// <param name="_TotalRow">Tổng số bản ghi</param>
        /// <Modified>        
        ///	Name		Date		    Comment 
        /// dongdt     10/11/2011      Tạo mới
        /// </Modified>
        public string getHtmlPageForum(string _LinkPage, int _PageStep, int _CurrentPage, int _RowPerPage, int _TotalRow)
        {
            this.PageStep = _PageStep;
            CurrentPage = _CurrentPage;
            LinkPage = _LinkPage;
            if (_RowPerPage == 0)
                _RowPerPage = 5;
            TotalPage = (_TotalRow % _RowPerPage == 0) ? _TotalRow / _RowPerPage : ((_TotalRow - (_TotalRow % _RowPerPage)) / _RowPerPage) + 1;
            return WriteHTMLPageForum();
        }
    }
}
