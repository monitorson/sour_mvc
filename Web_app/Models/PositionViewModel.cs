﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataAccess.Entities;
namespace Web_app.Models
{
    public class PositionViewModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}