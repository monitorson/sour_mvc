﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using DailyOpt_CROP4A.Validate;
using DataAccess.Entities;

namespace Web_app.Models
{
    public class AlbumViewModel
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Img { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public List<PositionEntity> Locations { get; set; } = new List<PositionEntity>();
    }
}