﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataAccess.Entities;
using System;

namespace Web_app.Models
{
    public class DonHangViewModel
    {
        public int? Id { get; set; }
        public string TenSanPham { get; set; }
        public int SoLuong { get; set; }
        public string DonGia { get; set; }
        public string TongGia { get; set; }
        public string TenKhachHang { get; set; }
        public string DiaChi { get; set; }
        public string Sdt { get; set; }
        public string GhiChu { get; set; }
        public string LoaiThanhToan { get; set; }
        public string NgayMua { get; set; }
        public bool Active { get; set; }
    }
}