﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataAccess.Entities;
namespace Web_app.Models
{
    public class MenuViewModel
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Link { get; set; }
        public string Detail { get; set; }
        public int? OrderMenu { get; set; }
        public string Location { get; set; }
        public string Img { get; set; }
        public int ParentId { get; set; }
        public int? Active { get; set; }
        public List<GroupNewsEntity> Urls { get; set; } = new List<GroupNewsEntity>();
        public List<MenuEntity> Parents { get; set; } = new List<MenuEntity>();
        

    }
}