﻿using Web_app.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataAccess.Entities;
namespace Web_app.Models
{
    public class UserAdminViewModel
    {
        public int? Id { get; set; }
        public string Username { get; set; }

        public string Password { get; set; }

        public string Fullname { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }
        public int? Role { get; set; }

        public string DisplayRole { get; set; }
        public List<RoleEntity> Roles { get; set; } = new List<RoleEntity>();
        
    }
}