﻿using Web_app.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web_app.Custom
{
    public abstract class AppWebViewPage<TModel> : WebViewPage<TModel>
    {
        public MenuHelperViewPages Menu { get; private set; }

        public override void InitHelpers()
        {
            base.InitHelpers();
            Menu = new MenuHelperViewPages(Html);
        }
    }
}